//
//  FirstScreen.swift
//  noonAcademyAssignment
//
//  Created by Apple on 22/06/17.
//  Copyright © 2017 noonAcademy. All rights reserved.
//

import UIKit

class FirstScreen: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var mainTableView: UITableView!
    var schoolSubjectsTitle = ["Science","Design and technology (ICT)","History","Geography","Art and design","Music","Physical Education","Religious education"]
    var subjectIcons = ["science","cube","history","geography","art","music","common","common"]
    var schoolSubjectDescription = ["Science","Design and technology (ICT)","History","Geography","Art and design","Music","Physical Education","Religious education" ]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mainTableView.delegate = self
        self.mainTableView.dataSource = self
        self.mainTableView!.registerNib(UINib(nibName: "schoolSubjectTableViewCell", bundle: nil), forCellReuseIdentifier: "schoolSubjectTableViewCell")

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
   return schoolSubjectsTitle.count
        
    }
//    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        
//        return 50
//    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("schoolSubjectTableViewCell", forIndexPath: indexPath) as! schoolSubjectTableViewCell
        cell.subjectTitle.text = schoolSubjectsTitle[indexPath.row]
        cell.subjectDescription.text = schoolSubjectDescription[indexPath.row]
        cell.subjectImg.image = UIImage.init(named: subjectIcons[indexPath.row])

        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
        
        
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 90
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
