//
//  schoolSubjectTableViewCell.swift
//  noonAcademyAssignment
//
//  Created by Apple on 22/06/17.
//  Copyright © 2017 noonAcademy. All rights reserved.
//

import UIKit

class schoolSubjectTableViewCell: UITableViewCell {

    @IBOutlet var subjectDescription: UILabel!
    @IBOutlet var subjectTitle: UILabel!
    @IBOutlet var subjectImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
